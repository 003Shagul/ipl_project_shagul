const csvtojson = require('csvtojson');
const fs = require('fs');

const matches = "../data/matches.csv";
const deliveries = "../data/deliveries.csv";

const matchesPlayedPerYear = require("./matchesPlayedPerYear");
const matchesWonByTeamPerYear = require("./matchesWonByTeamPerYear");
csvtojson()
  .fromFile(matches)
  .then((matchData) => {
    let matchesPlayedPerYearResult = matchesPlayedPerYear(matchData);
    //console.log(typeof matchesPlayedPerYearResult);

    matchesPlayedPerYearResult = JSON.stringify(matchesPlayedPerYearResult);
    //console.log(typeof matchesPlayedPerYearResult);

    fs.writeFile(
      "../public/output/matchesPlayedPerYearResult.json",
      matchesPlayedPerYearResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );
    
    let matchesWonByTeamPerYearResult = matchesWonByTeamPerYear(matchData);
    matchesWonByTeamPerYearResult = JSON.stringify(matchesWonByTeamPerYearResult);
    console.log(matchesWonByTeamPerYearResult);

    fs.writeFile(
      "../public/output/matchesWonByTeamPerYearResult.json",
      matchesWonByTeamPerYearResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );
});
